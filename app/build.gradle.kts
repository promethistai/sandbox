val ktorVersion by System.getProperties()

plugins {
    kotlin("multiplatform")
    application
}

kotlin {
    // native
    // see https://play.kotlinlang.org/hands-on/Introduction%20to%20Kotlin%20Native/02_CreatingAProject
    val osName = System.getProperty("os.name")
    val osArch = System.getProperty("os.arch")
    when {
        osName == "Mac OS X" -> when (osArch) { // uncomment when macosArm64 will be available
            "x86_64" -> macosX64("native")
            "aarch64" -> macosArm64("native")
            else -> null
        }
        osName == "Linux" -> when (osArch) {
            "x86_64", "amd64" -> linuxX64("native")
            "arm64" -> linuxArm64("native")
            else -> null
        }
        osName.startsWith("Windows") -> when (osArch) {
            "x86_64", "amd64" -> mingwX64("native")
            "x86" -> mingwX86("native")
            else -> null
        }
        else -> null
    }?.apply {
        binaries {
            executable {
                entryPoint = "ai.promethist.sandbox.main"
            }
        }

    } ?: throw GradleException("Host OS $osName ($osArch) is not supported in Kotlin/Native $project")

    jvm {
        compilations.all {
            kotlinOptions.jvmTarget = "1.8"
        }
        testRuns["test"].executionTask.configure {
            useJUnit()
        }
        withJava()
    }
    sourceSets {
        val jvmMain by getting {
            dependencies {
                implementation(project(":sandbox-lib"))
                implementation(project(":sandbox-javalib"))
                implementation("io.ktor:ktor-client-java:$ktorVersion")
            }
        }
        val jvmTest by getting {
            dependencies {
                implementation(kotlin("test"))
            }
        }
        /*
        val jsMain by getting {
            dependencies {
                implementation(project(":sandbox-lib"))
                implementation("io.ktor:ktor-client-js:$ktorVersion")
            }
        }
         */
        val nativeMain by getting {
            dependencies {
                implementation(project(":sandbox-lib"))
                implementation("io.ktor:ktor-client-curl:$ktorVersion")
            }
        }
        val nativeTest by getting
    }
}

application {
    mainClass.set("ai.promethist.sandbox.Main")
}

tasks.getByName<Jar>("jvmJar") {
    duplicatesStrategy = DuplicatesStrategy.WARN
    isZip64 = true
    manifest {
        attributes["Main-Class"] = application.mainClass
    }
    // create fat jar
    from(sourceSets.main.get().output)
    dependsOn(configurations.runtimeClasspath)
    from({
        configurations.runtimeClasspath.get().filter { it.name.endsWith("jar") }.map { zipTree(it) }
    })
}
