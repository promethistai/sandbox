package ai.promethist.sandbox

import ai.sandbox.IdWrapper
import kotlinx.coroutines.*

object Main {
    @JvmStatic
    fun main(args: Array<String>) {
        runBlocking {
            //val id = IdWrapper<String>(null)
            App.run("JAVA", args)
        }
    }
}