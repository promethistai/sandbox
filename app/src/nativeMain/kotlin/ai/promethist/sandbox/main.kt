package ai.promethist.sandbox

import kotlinx.coroutines.runBlocking

fun main(args: Array<String>) = runBlocking {
    App.run(Platform.osFamily.name, args)
}