dependencyResolutionManagement {
    repositories {
        mavenCentral()
        jcenter()
        mavenLocal()
    }
}

pluginManagement {
    plugins {
        val kotlinVersion: String by System.getProperties()

        id("kotlin-gradle-plugin") version kotlinVersion
        kotlin("multiplatform") version kotlinVersion
        kotlin("jvm") version kotlinVersion
    }
}

rootProject.name = "sandbox"

arrayOf("app", "lib", "javalib").forEach {
    val path = ":${rootProject.name}-$it"
    include(path)
    project(path).projectDir = file(it.replace('-', '/'))
}
