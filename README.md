
* publishing to local repo `./gradlew publishToMavenLocal -Pversion=x.y`
* publishing to maven repo (dir/url) `./gradlew publish -Pversion=x.y -PmavenRepositoryUri=/path/to/repo`
* dependencies between modules (see app)
* executable application
* build on M1 Mac (skip native - macosArm64 support not available yet): `./gradlew clean build -x linkDebugExecutableNative -x linkReleaseExecutableNative`