package ai.promethist.sandbox

object App {

    suspend fun run(platform: String, args: Array<String>) {
        println("run on $platform with args ${args.toList()}")
        val obj = WebClient.check()
        println(obj)
    }
}