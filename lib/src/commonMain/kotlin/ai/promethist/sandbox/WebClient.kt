package ai.promethist.sandbox

import io.ktor.client.*
import io.ktor.client.request.*
import io.ktor.http.*

object WebClient {

    data class Response(val health: Double, val name: String)

    suspend fun check() = HttpClient {

    }.get<String>("https://core.flowstorm.ai/check") {
        method = HttpMethod.Get
    }
}