package ai.promethist.sandbox

data class Response(val message: String)