val kotlinVersion by System.getProperties()
val ktorVersion by System.getProperties()

plugins {
    kotlin("multiplatform")
    id("maven-publish") // see https://docs.gradle.org/current/userguide/publishing_maven.html
}

kotlin {
    jvm {
        compilations.all {
            kotlinOptions.jvmTarget = "1.8"
        }
        testRuns["test"].executionTask.configure {
            useJUnit()
        }
        withJava()
    }
//    js {
//        browser()
//    }
    val osName = System.getProperty("os.name")
    val osArch = System.getProperty("os.arch")
    when {
        osName == "Mac OS X" -> when (osArch) { // uncomment when macosArm64 will be available
            "x86_64" -> macosX64("native")
            "aarch64" -> macosArm64("native")
            else -> null
        }
        osName == "Linux" -> when (osArch) {
            "x86_64", "amd64" -> linuxX64("native")
            "arm64" -> linuxArm64("native")
            else -> null
        }
        osName.startsWith("Windows") -> when (osArch) {
            "x86_64", "amd64" -> mingwX64("native")
            "x86" -> mingwX86("native")
            else -> null
        }
        else -> null
    } ?: throw GradleException("Host OS $osName is not supported in Kotlin/Native $project")

    sourceSets {
        val commonMain by getting {
            dependencies {
                implementation("io.ktor:ktor-client-core:$ktorVersion")
                //implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.4.2")
            }
        }
        val commonTest by getting {
            dependencies {
                implementation(kotlin("test"))
            }
        }
//        val jsMain by getting
//        val jsTest by getting
        val jvmMain by getting
        val jvmTest by getting {
            dependencies {
                implementation(kotlin("test"))
            }
        }
        val nativeMain by getting
        val nativeTest by getting
    }
}

publishing {
    repositories {
        maven {
            val mavenRepositoryUri by System.getProperties()
            url = uri(mavenRepositoryUri)
        }
    }
}
