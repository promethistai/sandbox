import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

val kotlinVersion: String by System.getProperties()

plugins {
    kotlin("jvm")
    id("maven-publish") // see https://docs.gradle.org/current/userguide/publishing_maven.html
}

dependencies {
    implementation("org.litote.kmongo:kmongo-id:3.12.2")
}

publishing {
    repositories {
        maven {
            val mavenRepositoryUri by System.getProperties()
            url = uri(mavenRepositoryUri)
        }
    }
}

tasks.withType<KotlinCompile> {
    val javaVersion: String by System.getProperties()
    kotlinOptions.jvmTarget = javaVersion
}

